package com.yg.app.travel.controller;

import com.alibaba.fastjson.JSONArray;
import com.yg.app.travel.common.CommonConstant;
import com.yg.app.travel.service.BaseService;
import com.yg.app.travel.utils.BlankCheckUtil;
import com.yg.app.travel.vo.Trequest;
import com.yg.app.travel.vo.Tresponse;
import com.yg.app.travel.vo.req.*;
import com.yg.app.travel.vo.resp.RespSenicDetail;
import com.yg.app.travel.vo.travel.*;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/26 16:14
 * @file: CommonController
 * @description: 星期一
 */
@RestController
@RequestMapping("sys")
@CrossOrigin
public class CommonController {

    @Autowired
    BaseService baseService;

    @ApiOperation(tags = "查询景点信息接口", value = "/query_scenic_area_info")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "scenicName ", value = "景点名称", dataType = "string", paramType = "query", example = "绵阳科技馆"),
            @ApiImplicitParam(name = "isHot ", value = "是否热门景点", dataType = "string", paramType = "query", example = "任意字符串，不为空就行"),
    })
    @RequestMapping(path = CommonConstant.QUERY_SCENIC_AREA_INFO, method = RequestMethod.POST)
    public Tresponse<List<Scenic>> queryScenicInfo(@RequestBody() Trequest<ReqSenicInfo> request) {
        BlankCheckUtil.isBlank(request,"参数错误");
        System.out.println("request-data:"+ JSONArray.toJSONString(request.getData()));
        return baseService.queryScnicPage(request.getData());
    }

    @ApiOperation(tags = {"查询景点详细信息接口"}, value = "/query_scenic_area_detail_info")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id  ", value = "景点id", dataType = "int", paramType = "query", example = "15"),
    })
    @RequestMapping(path = CommonConstant.QUERY_SCENIC_AREA_DETAIL_INFO, method = RequestMethod.POST)

    public Tresponse<RespSenicDetail> queryScenicDetail(@RequestBody() Trequest<Integer> request) {
        BlankCheckUtil.isBlank(request, request.getData(), "参数错误");
        return baseService.querySceicDetail(request.getData());
    }

    @ApiOperation(tags = {"周边游信息接口"}, value = "/query_travel_area_around")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "lng", value = "当前所在位置经度", dataType = "double", paramType = "query", example = "102.45625"),
            @ApiImplicitParam(name = "lat", value = "当前所在位置纬度", dataType = "double", paramType = "query", example = "31.23564"),
            @ApiImplicitParam(name = "type", value = "周边游类型 0 全部 1 四小时游 2 一日游 3 两日游", dataType = "int", paramType = "query", example = "1"),
    })
    @RequestMapping(path = "/query_travel_area_around",method = RequestMethod.POST)
    public Tresponse<List<Scenic>> queryTravelAround(@RequestBody() Trequest<ReqTravelAround> request){
        BlankCheckUtil.isBlank(request,request.getData(),"参数错误");
        return baseService.queryTravelAround(request.getData());
    }

    @ApiOperation(tags = {"查询优惠卷信息接口"}, value = "/query_coupon_info")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId ", value = "用户名", dataType = "string", paramType = "query", example = "微信登陆OpenId",required = true),
            @ApiImplicitParam(name = "couponName", value = "优惠卷名称", dataType = "string", paramType = "query", example = "七天假日酒店七折卡"),
            @ApiImplicitParam(name = "couponMoney ", value = "优惠卷金额", dataType = "float", paramType = "query", example = "50.5")
    })
    @RequestMapping(path = CommonConstant.QUERY_COUPON_INFO, method = RequestMethod.POST)

    public Tresponse<List<Coupon>> queryCouponInfo(@RequestBody() Trequest<ReqCouponInfo> request) {
        BlankCheckUtil.isBlank(request, request.getData(), "参数错误");
        return baseService.queryCouponPage(request.getData());
    }

    @ApiOperation(tags = {"查询订单信息接口"}, value = "/query_order_info")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId ", value = "用户ID", dataType = "int", paramType = "query", example = "yg"),
            @ApiImplicitParam(name = "payable", value = "支付状态", dataType = "int", paramType = "query", example = "0"),
    })
    @RequestMapping(path = CommonConstant.QUERY_ORDER_INFO, method = RequestMethod.POST)
    public Tresponse<List<Order>> queryOrderInfo(@RequestBody() Trequest<ReqOrderInfo> request) {
        BlankCheckUtil.isBlank(request, request.getData(), "参数错误");
        return baseService.queryOrderPage(request.getData());
    }

    @ApiOperation(tags = {"新建订单接口"},value = "/create_order_info")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderName",value = "订单名称",dataType = "string",paramType = "insert",example = "联想拯救者Y7000"),
            @ApiImplicitParam(name = "orderTime",value = "下单时间",dataType = "timstamp",paramType = "insert",example = "2020-08-07 15:02:30"),
            @ApiImplicitParam(name = "travelTime",value = "旅程时间",dataType = "string",paramType = "insert",example ="2020-08-15"),
            @ApiImplicitParam(name = "accountActual",value = "应付价格",dataType = "float",paramType = "insert",example = "4000.0"),
            @ApiImplicitParam(name = "userId",value = "用户名(openId)",dataType = "string",paramType = "insert",example = "虚空之女"),
            @ApiImplicitParam(name = "couponId",value = "优惠卷ID",dataType = "int",paramType = "query",example = "5")
    })
    @RequestMapping(path = "create_order_info",method = RequestMethod.POST)
    public Tresponse<Integer> createOrderInfo(@RequestBody() Trequest<Order> request) throws Exception {
        BlankCheckUtil.isBlank(request, request.getData(), "参数错误");
        return baseService.createOrderInfo(request.getData());
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name="orderId",value ="订单ID",dataType = "int",paramType = "query",example = "1"),
    })
    @ApiOperation(tags = {"确定支付接口"},value = "/pay_actual")
    @RequestMapping(path = "pay_actual",method = RequestMethod.POST)
    public Tresponse<Integer> payActual(@RequestBody() Trequest<Integer> request){
        BlankCheckUtil.isBlank(request, request.getData(), "参数错误");
        return baseService.payActual(request.getData());
    }

    @ApiOperation(tags = {"删除订单信息接口"}, value = "/delete_order")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderId ", value = "订单id", dataType = "int", paramType = "query", example = "15"),
    })
    @RequestMapping(path = CommonConstant.DELETE_ORDER, method = RequestMethod.POST)
    public Tresponse<Integer> deleteOrder(@RequestBody() Trequest<Integer> request) {
        BlankCheckUtil.isBlank(request, request.getData(), "参数错误");
        return baseService.deleteOrderById(request.getData());
    }

    @ApiOperation(tags = {"删除优惠卷信息接口"}, value = "/delete_coupon")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "couponId ", value = "优惠卷id", dataType = "int", paramType = "query", example = "15"),
    })
    @RequestMapping(path = CommonConstant.DELETE_COUPON, method = RequestMethod.POST)
    public Tresponse<Integer> deleteCoupon(@RequestBody() Trequest<Integer> request) {
        BlankCheckUtil.isBlank(request, request.getData(), "参数错误");
        return baseService.deleteById(request.getData());
    }

    @ApiOperation(tags = "评论接口",value = "/coment")
    @RequestMapping(path = "comment",method = RequestMethod.POST)
    public Tresponse<Integer> comment(@RequestBody() Trequest<Comment> request){
        BlankCheckUtil.isBlank(request,request.getData(),"参数错误");
        return baseService.comment(request.getData());
    }

    @ApiOperation(tags = "查询攻略接口",value = "/strategy")
    @RequestMapping(path = "strategy",method = RequestMethod.POST)
    public Tresponse<List<Strategy>> queryStrategy(){
        return baseService.queryStrategy();
    }
}
