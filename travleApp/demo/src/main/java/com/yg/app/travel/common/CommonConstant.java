package com.yg.app.travel.common;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/26 16:19
 * @file: CommonConstant
 * @description: 星期一
 */
public class CommonConstant {

    public static final String ACCESS_TOKEN = "Access-Token";
    /**
     * 查询景点信息
     **/
    public static final String QUERY_SCENIC_AREA_INFO = "/query_scenic_area_info";
    /**
     * 查询景点详细信息
     **/
    public static final String QUERY_SCENIC_AREA_DETAIL_INFO = "/query_scenic_area_detail_info";
    /**
     * 查询酒店信息
     **/
    public static final String QUERY_HOTEL_INFO = "/query_hotel_info";
    /**
     * 查询详细信息
     **/
    public static final String QUERY_HOTEL_DETAIL_INFO = "/query_hotel_detail_info";
    /**
     * 查询订单信息
     **/
    public static final String QUERY_ORDER_INFO = "/query_order_info";
    /**
     * 查询优惠卷信息
     **/
    public static final String QUERY_COUPON_INFO = "/query_coupon_info";
    /**
     * 查询房间详细信息
     **/
    public static final String QUERY_HOUSE_INFO = "/query_house_info";
    /**
     * 登录
     **/
     public static final String LOGIN = "/login";

    /**
     * 注册
     **/
    public static final String REGIST = "/regist";

    public static final String DELETE_ORDER="/delete_order";

    public static final String DELETE_COUPON="/delete_coupon";
}
