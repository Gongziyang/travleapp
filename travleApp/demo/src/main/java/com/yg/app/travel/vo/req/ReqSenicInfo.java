package com.yg.app.travel.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/25 21:27
 * @file: ReqSenicInfo
 * @description: 查询风景信息实体
 */
@ApiModel(value = "查询景区信息所需携带的信息")
public class ReqSenicInfo extends ReqPage{


    /**
     * 是否查询热门景点
     * **/
    @ApiModelProperty(value = "是否查询热门景点")
    private String isHot;

    /**
     * 景点名称
     ***/
    @ApiModelProperty(value = "景点名称")
    private String scenicName;


    public String getScenicName() {
        return scenicName;
    }

    public void setScenicName(String scenicName) {
        this.scenicName = scenicName;
    }

    public String getIsHot() {
        return isHot;
    }

    public void setIsHot(String isHot) {
        this.isHot = isHot;
    }
}
