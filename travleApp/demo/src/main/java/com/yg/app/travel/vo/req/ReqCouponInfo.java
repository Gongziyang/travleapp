package com.yg.app.travel.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/26 14:43
 * @file: ReqCouponInfo
 * @description: 查看优惠卷实体
 */
@ApiModel(value = "查看优惠卷信息所需携带的信息体")
public class ReqCouponInfo extends ReqPage {

    /**
     * 优惠卷名称
     **/
    @ApiModelProperty(value = "优惠卷名称")
    private String couponName = null;

    /**
     * 优惠卷金额
     **/
    @ApiModelProperty(value = "优惠卷金额")
    private float couponMoney = -1;
    /**
     * 用户Id,
     **/
    @ApiModelProperty(value = "用户名,必带字段")
    private String userId;


    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public float getCouponMoney() {
        return couponMoney;
    }

    public void setCouponMoney(float couponMoney) {
        this.couponMoney = couponMoney;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
