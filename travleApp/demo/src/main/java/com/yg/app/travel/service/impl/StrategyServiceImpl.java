package com.yg.app.travel.service.impl;

import com.yg.app.travel.mapper.StrategyMapper;
import com.yg.app.travel.service.StrategyService;
import com.yg.app.travel.vo.travel.Strategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/1 19:17
 * @file: StrategyServiceImpl
 * @description: 星期二
 */
@Service
public class StrategyServiceImpl implements StrategyService {

    @Autowired
    StrategyMapper strategyMapper;

    @Override
    public List<Strategy> queryStrategy() {
        return strategyMapper.queryStrategy();
    }
}
