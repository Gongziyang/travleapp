package com.yg.app.travel.vo;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/25 21:21
 * @file: Tresponse
 * @description: 星期日
 */
public class Tresponse<T> {

   private int itemSize;

   private int pageCount;

   private T data;

    public int getItemSize() {
        return itemSize;
    }

    public void setItemSize(int itemSize) {
        this.itemSize = itemSize;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
