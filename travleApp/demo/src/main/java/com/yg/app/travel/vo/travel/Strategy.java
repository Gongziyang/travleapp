package com.yg.app.travel.vo.travel;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/1 19:08
 * @file: Strategy
 * @description: 星期二
 */
public class Strategy {

    private int id;

    private String url;

    private String info;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
