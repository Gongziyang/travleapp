package com.yg.app.travel.service.impl;

import com.yg.app.travel.mapper.CouponMapper;
import com.yg.app.travel.service.CouponService;
import com.yg.app.travel.vo.req.ReqCouponInfo;
import com.yg.app.travel.vo.travel.Coupon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/26 16:12
 * @file: CouponServiceImpl
 * @description: 星期一
 */
@Service
public class CouponServiceImpl implements CouponService {

    @Autowired
    CouponMapper couponMapper;

    @Override
    public List<Coupon> queryCouponPage(ReqCouponInfo request) {
        return couponMapper.queryCouponPage(request);
    }

    @Override
    public int deleteById(int id) {
        return couponMapper.deleteById(id);
    }

    @Override
    public int itemSize(ReqCouponInfo request) {
        return couponMapper.itemSize(request);
    }

    @Override
    public int insert(Coupon coupon) {
        return couponMapper.insertCoupon(coupon);
    }

    @Override
    public int update(int id) {
        return couponMapper.update(id);
    }

    @Override
    public Coupon getById(Integer id) {
        return couponMapper.getById(id);
    }


}
