package com.yg.app.travel.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.AbstractFileResolvingResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.sql.DataSource;
import java.io.IOException;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/25 21:07
 * @file: DataSourceConfig
 * @description: 星期日
 */
@Configuration //表明该类为配置类
public class DataSourceConfig {

    @Value("${spring.datasource.url}")
    private String url;
    @Value("${spring.datasource.driver-class-name}")
    private String driver;
    @Value("${spring.datasource.username}")
    private String username;
    @Value("${spring.datasource.password}")
    private String password;

    @Bean
    public SqlSessionFactory sqlSessionFactoryBean() throws Exception {
          SqlSessionFactoryBean sessionFactoryBean = new SqlSessionFactoryBean();
          sessionFactoryBean.setDataSource(dataSource());
        Resource[] mappers = new PathMatchingResourcePatternResolver()
                .getResources("classpath:mapper/**/*.xml");
        Resource mybatis_config = new PathMatchingResourcePatternResolver()
                .getResource("classpath:mybatis-config.xml");
        sessionFactoryBean.setConfigLocation(mybatis_config);
        sessionFactoryBean.setMapperLocations(mappers);

        return sessionFactoryBean.getObject();
    }

    @Bean
    public DataSource dataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setPassword(password);
        dataSource.setUsername(username);
        dataSource.setUrl(url);
        dataSource.setDriverClassName(driver);
        return dataSource;

    }
}
