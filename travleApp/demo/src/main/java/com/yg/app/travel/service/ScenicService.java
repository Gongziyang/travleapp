package com.yg.app.travel.service;

import com.yg.app.travel.vo.req.ReqSenicInfo;
import com.yg.app.travel.vo.req.ReqTravelAround;
import com.yg.app.travel.vo.resp.RespSenicDetail;
import com.yg.app.travel.vo.travel.Comment;
import com.yg.app.travel.vo.travel.Scenic;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/26 16:12
 * @file: ScenicService
 * @description: 星期一
 */
public interface ScenicService {

    /**
     * 分页查询景点信息
     * **/
    List<Scenic> queryScnicPage(ReqSenicInfo request);

    /**
     * 查询景点详情，包括用户的评论信息
     * @param id 景点id
     * **/
    RespSenicDetail querySceicDetail(int id);

    int itemSize(ReqSenicInfo request);

    List<Scenic> queryTravelAround(Integer sorted);

    int comment(Comment comment);
}
