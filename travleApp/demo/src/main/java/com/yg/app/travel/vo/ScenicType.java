package com.yg.app.travel.vo;

/**
 * 2.  * @Description:
 * 3.  * @Copyright:      Copyright (c) 2019  ALL RIGHTS RESERVED.
 * 4.  * @Company:        旺小宝
 * 5.  * @Author:         杨一峰
 * 6.  * @CreateDate:     2021/6/28 14:39
 * 7.  * @UpdateDate:     2021/6/28 14:39
 * 8.  * @UpdateRemark:   init
 * 9.  * @Version:         1.0
 * 10.  * @menu           景点枚举类
 * 11.
 */
public enum ScenicType {

    HotArea("1","热门景点"),
    PopularArea("2","流行景点");

    private String id;
    private String msg;
    private  ScenicType(String id,String msg){
        this.id = id;
        this.msg = msg;
    }
}
