package com.yg.app.travel.mapper;

import com.yg.app.travel.vo.req.ReqCouponInfo;
import com.yg.app.travel.vo.travel.Coupon;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;


import java.util.List;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/26 16:11
 * @file: CouponMapper
 * @description: 星期一
 */
@Mapper
@Repository
public interface CouponMapper {

    /**
     * 分页查询优惠卷
     * **/
    List<Coupon> queryCouponPage(ReqCouponInfo request);

    /**
     * 删除优惠卷
     * @param id 优惠卷id
     * **/
    int deleteById(int id);

    int itemSize(ReqCouponInfo request);

    int insertCoupon(Coupon coupon);

    int update(int id);

    Coupon getById(Integer id);
}
