package com.yg.app.travel.service;

import com.yg.app.travel.vo.Tresponse;
import com.yg.app.travel.vo.req.*;
import com.yg.app.travel.vo.resp.RespSenicDetail;
import com.yg.app.travel.vo.travel.*;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/27 9:40
 * @file: BaseService
 * @description: 星期二
 */
public interface BaseService {

    /**
     * 分页查询优惠卷
     * **/
    Tresponse<List<Coupon>> queryCouponPage(ReqCouponInfo request);

    /**
     * 删除优惠卷
     * @param id 优惠卷id
     * **/
    Tresponse<Integer> deleteById(int id);

    /**
     * 分页查询订单信息
     * **/
    Tresponse<List<Order>> queryOrderPage(ReqOrderInfo request);

    /**
     * 通过id删除订单
     * @param orderId 订单id
     * **/
    Tresponse<Integer> deleteOrderById(int orderId);

    /**
     * 分页查询景点信息
     * **/
    Tresponse<List<Scenic>> queryScnicPage(ReqSenicInfo request);

    /**
     * 查询景点详情，包括用户的评论信息
     * @param id 景点id
     * **/
    Tresponse<RespSenicDetail> querySceicDetail(int id);


    Tresponse<List<Scenic>> queryTravelAround(ReqTravelAround reqTravelAround);

    Tresponse<Integer> createOrderInfo(Order order) throws Exception;

    Tresponse<Integer> payActual(Integer orderId);

    Tresponse<Integer> comment(Comment comment);

    Tresponse<List<Strategy>> queryStrategy();
}
