package com.yg.app.travel.utils;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
/**
 * @author: yyf
 * @create: 2021 04 2021/4/20 13:48
 * @file: BlankCheckUtil
 * @description: 星期二
 */
public class BlankCheckUtil {

    /**
     * 检查字符串是否为空
     * **/
    public static void isBlank(String param,String message){
        if(StrUtil.isBlank(param)){
        throw new RuntimeException(message);
        }
    }


    /**
     *检查字符串数组是否为空，只要有一个为空都报错
     */
    public static  void isBlankArray(String message,String... strings){
       if(ArrayUtil.isEmpty(strings)){
           throw new RuntimeException(message);
       }
       for(String s : strings){
           isBlank(s,message);
       }
    }

    /**检验对象是否为空**/
    public static void isBlank(Object o,String message){
        if(o == null){
            throw  new RuntimeException(message);
        }
        if(StrUtil.isBlank(o.toString())){
             throw new RuntimeException(message);
         }

    }

    public static void isBlank(Object obj1,Object obj2,String message){
        System.out.println("[obj1]"+obj1);
        System.out.println("[obj2]"+obj2);
        boolean f = true;
         if( obj1 == null){
             f = false;
         }
         if(obj2 == null){
             f = false;
         }
         if(!f){
             throw new RuntimeException(message);
         }
    }
}
