package com.yg.app.travel.service;

import com.yg.app.travel.vo.travel.Strategy;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/1 19:17
 * @file: StrategyService
 * @description: 星期二
 */
public interface StrategyService {

    List<Strategy> queryStrategy();
}
