package com.yg.app.travel.service;

import com.yg.app.travel.vo.req.ReqOrderInfo;
import com.yg.app.travel.vo.travel.Order;

import java.util.List;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/26 16:12
 * @file: OrderService
 * @description: 星期一
 */
public interface OrderService {

    /**
     * 分页查询订单信息
     * **/
    List<Order> queryOrderPage(ReqOrderInfo request);

    /**
     * 通过id删除订单
     * @param orderId 订单id
     * **/
    int deleteOrderById(int orderId);

    int itemSize(ReqOrderInfo request);

    int createOrderInfo(Order order);

    Order queryOrderById(Integer orderId);

    int payActual(Map orderInfo);
}
