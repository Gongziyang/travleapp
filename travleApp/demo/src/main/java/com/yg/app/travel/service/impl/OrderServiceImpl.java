package com.yg.app.travel.service.impl;

import com.yg.app.travel.mapper.OrderMapper;
import com.yg.app.travel.service.OrderService;
import com.yg.app.travel.utils.PasswordUtil;
import com.yg.app.travel.vo.req.ReqOrderInfo;
import com.yg.app.travel.vo.travel.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/26 16:13
 * @file: OrderServiceImpl
 * @description: 星期一
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderMapper orderMapper;

    @Override
    public List<Order> queryOrderPage(ReqOrderInfo request) {
        return orderMapper.queryOrderPage(request);
    }

    @Override
    public int deleteOrderById(int orderId) {
        return orderMapper.deleteOrderById(orderId);
    }

    @Override
    public int itemSize(ReqOrderInfo request) {
        return orderMapper.itemSize(request);
    }

    @Override
    public int createOrderInfo(Order order) {
        return orderMapper.createOrderInfo(order);
    }

    @Override
    public Order queryOrderById(Integer orderId) {
        return orderMapper.queryOrderById(orderId);
    }

    @Override
    public int payActual(Map orderInfo) {
        return orderMapper.payActual(orderInfo);
    }


}
