package com.yg.app.travel.aop;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/27 9:52
 * @file: ExceptionHandle
 * @description: 星期二
 */
//@ControllerAdvice
public class ExceptionHandle {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Map<String,Object> exceptionHandle(Exception e){
        Map<String,Object> params = new HashMap<>(3);
        params.put("Ret",1);
        params.put("Msg",e.getMessage());
        params.put("Data",null);
        return params;
    }
}
