package com.yg.app.travel.service.impl;

import com.github.pagehelper.PageHelper;
import com.yg.app.travel.service.*;
import com.yg.app.travel.utils.BlankCheckUtil;
import com.yg.app.travel.utils.DistanceUtil;
import com.yg.app.travel.utils.PasswordUtil;
import com.yg.app.travel.vo.Tresponse;
import com.yg.app.travel.vo.req.*;
import com.yg.app.travel.vo.resp.RespSenicDetail;
import com.yg.app.travel.vo.travel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/27 9:41
 * @file: BaseServiceImpl
 * @description: 星期二
 */
@Service
public class BaseServiceImpl implements BaseService {

    public static int count = 0;
    public static String orderNumber = "ADGDRDHTWKO";

    @Autowired
    ScenicService scenicService;

    @Autowired
    CouponService couponService;

    @Autowired
    OrderService orderService;

    @Autowired
    StrategyService strategyService;

    Logger logger = LoggerFactory.getLogger(BaseServiceImpl.class);

    @Override
    public Tresponse<List<Coupon>> queryCouponPage(ReqCouponInfo request) {
        int pageNo = request.getPageNo();
        int pageSize = request.getPageSize();
        if (pageSize == 0) {
            pageNo = 1;
            pageSize = 5;
        }
        int itemSize = couponService.itemSize(request);
        int pageCount = itemSize % pageSize == 0 ? (itemSize / pageSize) : (itemSize / pageSize) + 1;
        logger.info("pageCount:{}", pageCount);
        if (pageNo == 0) {
            PageHelper.startPage(1, pageSize);
        } else {
            PageHelper.startPage(pageNo, pageSize);
        }
        List<Coupon> coupons = couponService.queryCouponPage(request);
        BlankCheckUtil.isBlank(coupons, "查询失败");
        Tresponse<List<Coupon>> response = new Tresponse<>();
        response.setData(coupons);
        response.setPageCount(pageCount);
        response.setItemSize(itemSize);
        return response;
    }

    /**
     * 通过ID删除优惠卷
     **/
    @Override
    public Tresponse<Integer> deleteById(int id) {
        Tresponse<Integer> response = new Tresponse<>();
        int result = couponService.deleteById(id);
        if (result == 0) {
            response.setData(-1);
        } else {
            response.setData(result);
        }
        return response;
    }


    @Override
    public Tresponse<List<Order>> queryOrderPage(ReqOrderInfo request) {
        int pageNo = request.getPageNo();
        int pageSize = request.getPageSize();
        if (pageSize == 0) {
            pageNo = 1;
            pageSize = 5;
        }
        int itemSize = orderService.itemSize(request);
        int pageCount = itemSize % pageSize == 0 ? (itemSize / pageSize) : (itemSize / pageSize) + 1;
        if (pageNo == 0) {
//            PageHelper.startPage(1, pageSize);
        } else {
//            PageHelper.startPage(pageNo, pageSize);
        }
        List<Order> orders = orderService.queryOrderPage(request);
        BlankCheckUtil.isBlank(orders, "查询失败");
        Tresponse<List<Order>> response = new Tresponse<>();
        response.setData(orders);
        response.setPageCount(pageCount);
        response.setItemSize(itemSize);
        return response;
    }

    @Override
    public Tresponse<Integer> deleteOrderById(int orderId) {

        Tresponse<Integer> response = new Tresponse<>();
        int result = orderService.deleteOrderById(orderId);
        if (result == 0) {
            response.setData(-1);
        } else {
            response.setData(1);
        }
        return response;
    }

    @Override
    public Tresponse<List<Scenic>> queryScnicPage(ReqSenicInfo request) {
        int pageNo = 0;
        int pageSize = 0;
        if (request == null) {
            pageSize = 10;
            pageNo = 1;
        } else {
            pageNo = request.pageNo;
            pageSize = request.getPageSize();
        }
        int itemSize = scenicService.itemSize(request);
        if (pageSize == 0) {
            pageSize = itemSize;
            pageNo = 1;
        }
        int pageCount = itemSize % pageSize == 0 ? (itemSize / pageSize) : (itemSize / pageSize) + 1;
        if (pageNo == 0) {
            PageHelper.startPage(1, pageSize);
        } else {
            PageHelper.startPage(pageNo, pageSize);
        }
        List<Scenic> scenics = scenicService.queryScnicPage(request);
        Tresponse<List<Scenic>> response = new Tresponse<>();
        response.setData(scenics);
        response.setPageCount(pageCount);
        response.setItemSize(itemSize);
        return response;
    }

    @Override
    public Tresponse<RespSenicDetail> querySceicDetail(int id) {
        Tresponse<RespSenicDetail> response = new Tresponse<>();
        RespSenicDetail respSenicDetail = scenicService.querySceicDetail(id);
        if (respSenicDetail == null) {
            response.setData(null);
            response.setPageCount(0);
            response.setItemSize(0);
        } else {
            response.setItemSize(1);
            response.setPageCount(1);
            response.setData(respSenicDetail);
        }
        return response;
    }


    @Override
    public Tresponse<List<Scenic>> queryTravelAround(ReqTravelAround reqTravelAround) {
        //当前坐标经度
        double lng = reqTravelAround.getLng();
        //当前坐标纬度
        double lat = reqTravelAround.getLat();
        int type = reqTravelAround.getType();
        double[] point1 = new double[]{lng, lat};
        double[] point2;
        List<Scenic> scenics = scenicService.queryTravelAround(reqTravelAround.getSorted());
        List<Scenic> travelScenics = new ArrayList<>(scenics.size());
        int start = 0;
        int end = 0;
        if (type == 0) {
            start = 0;
            end = Integer.MAX_VALUE;
        } else if (type == 1) {
            start = 0;
            end = 10;
        } else if (type == 2) {
            start = 11;
            end = 35;
        } else if (type == 3) {
            start = 36;
            end = Integer.MAX_VALUE;
        }
        for (Scenic s : scenics) {
            double lng1 = s.getLng();
            double lat1 = s.getLat();
            point2 = new double[]{lng1, lat1};
            float distance = DistanceUtil.getDistance(point1, point2);
            if (distance >= start && distance <= end) {
                travelScenics.add(s);
            }
        }
        Tresponse<List<Scenic>> response = new Tresponse<>();
        response.setData(travelScenics);
        response.setItemSize(travelScenics.size());
        return response;
    }

    @Override
    public Tresponse<Integer> createOrderInfo(Order order) throws Exception {
        Tresponse<Integer> response = new Tresponse<>();
        int couponId = order.getCouponId();
        if (couponId != 0) {
            Coupon coupon = couponService.getById(couponId);
            order.setCouponMoney(coupon.getCouponMoney());
            order.setWeatherUseCoupon(1);
        }
        order.setOrderNumber(orderNumber + count++);
        order.setOrderInfo(order.toString());
        int result = orderService.createOrderInfo(order);
        if (result == 0) {
            response.setData(-1);
            return response;
        }
        response.setData(order.getId());
        return response;
    }

    @Override
    public Tresponse<Integer> payActual(Integer orderId) {
        Order order = orderService.queryOrderById(orderId);

        int couponId = order.getCouponId();
        if (couponId != 0) {
            couponService.update(couponId);
        }
        //将支付状态设置为已支付
        order.setState(1);
        order.setOrderInfo(order.toString());
        Map<String, Object> map = new HashMap<>(2);
        map.put("orderInfo", order.getOrderInfo());
        map.put("id", orderId);
        int result = orderService.payActual(map);
        Tresponse<Integer> response = new Tresponse<>();
        if (result == 0) {
            response.setData(-1);
            return response;
        }
        response.setData(result);
        return response;
    }

    @Override
    public Tresponse<Integer> comment(Comment comment) {
        int result = scenicService.comment(comment);
        Tresponse<Integer> response = new Tresponse<>();
        if (result == 0) {
            response.setData(-1);
            return response;
        }
        response.setData(result);
        return response;
    }

    @Override
    public Tresponse<List<Strategy>> queryStrategy() {
        Tresponse<List<Strategy>> response = new Tresponse<>();
        List<Strategy> strategies = strategyService.queryStrategy();
        response.setData(strategies);
        if(strategies!=null){
            response.setItemSize(strategies.size());
        }
        return response;
    }
}
