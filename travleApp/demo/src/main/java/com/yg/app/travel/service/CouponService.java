package com.yg.app.travel.service;

import com.yg.app.travel.vo.req.ReqCouponInfo;
import com.yg.app.travel.vo.travel.Coupon;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/26 16:12
 * @file: CouponService
 * @description: 星期一
 */
public interface CouponService {

    /**
     * 分页查询优惠卷
     * **/
    List<Coupon> queryCouponPage(ReqCouponInfo request);

    /**
     * 删除优惠卷
     * @param id 优惠卷id
     * **/
    int deleteById(int id);

    int itemSize(ReqCouponInfo request);

    int insert(Coupon coupon);

    int update(int id);

    Coupon getById(Integer id);
}
