package com.yg.app.travel.vo;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/25 21:20
 * @file: Trequest
 * @description: 星期日
 */
public class Trequest<T> {

     private String timeStamp;

     private T data;

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
