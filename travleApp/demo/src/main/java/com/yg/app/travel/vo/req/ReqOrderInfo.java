package com.yg.app.travel.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/26 13:41
 * @file: ReqOrderInfo
 * @description: 查询订单信息实体
 */
@ApiModel(value = "查询订单信息所需要携带的信息体")
public class ReqOrderInfo extends ReqPage{

    /**
     * 用户ID
     **/
    @ApiModelProperty(value = "用户ID")
    private String  userId = null;

    /**
     * 订单状态
     * **/
    @ApiModelProperty(value = "订单状态")
    private int payable = -1;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getPayable() {
        return payable;
    }

    public void setPayable(int payable) {
        this.payable = payable;
    }
}
