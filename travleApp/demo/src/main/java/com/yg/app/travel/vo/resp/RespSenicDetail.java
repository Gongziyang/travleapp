package com.yg.app.travel.vo.resp;

import com.yg.app.travel.vo.travel.Comment;
import com.yg.app.travel.vo.travel.Coupon;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/25 21:24
 * @file: Senic
 * @description: 景点详情实体类
 */
@ApiModel(value = "景点详情信息，包含游客对景点的评价列表")
public class RespSenicDetail {

    private int id;

    /**
     * 景点名称
     **/
    @ApiModelProperty(value = "景点名称")
    private String scenicName;

    /**
     * 景点状态
     **/
    @ApiModelProperty(value = "景点状态")
    private int state;

    /**
     * 推荐指数
     **/
    @ApiModelProperty(value = "推荐指数")
    private int recommendRate;
    /**
     * 景点门票
     **/
    @ApiModelProperty(value = "景点门票")
    private float ticket;
    /**
     * 景点经度
     **/
    @ApiModelProperty(value = "景点经度")
    private float lng;
    /**
     * 景点纬度
     **/
    @ApiModelProperty(value = "景点纬度")
    private float lat;
    /**
     * 景区图片，url
     **/
    @ApiModelProperty(value = "景区图片，url")
    private String picture;

    /**
     * 详细地址
     * **/
    @ApiModelProperty(value = "详细地址")
    private String address;

    /**
     * 开放时间
     * **/
    @ApiModelProperty(value = "开放时间")
    private String openTime;

    /**
     * 景点介绍
     * **/
    @ApiModelProperty(value = "景点介绍")
    private String introduce;


    /**
     * 景点特色
     * **/
    @ApiModelProperty(value = "景点特色")
    private String feature;

    /**
     * 景区关联评论
     * **/
    @ApiModelProperty(value = "景区关联评论")
    private List<Comment> commentList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getScenicName() {
        return scenicName;
    }

    public void setScenicName(String scenicName) {
        this.scenicName = scenicName;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getRecommendRate() {
        return recommendRate;
    }

    public void setRecommendRate(int recommendRate) {
        this.recommendRate = recommendRate;
    }

    public float getTicket() {
        return ticket;
    }

    public void setTicket(float ticket) {
        this.ticket = ticket;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

}
