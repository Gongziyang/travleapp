package com.yg.app.travel.utils;

import org.aspectj.lang.annotation.Pointcut;
import org.gavaghan.geodesy.Ellipsoid;
import org.gavaghan.geodesy.GeodeticCalculator;
import org.gavaghan.geodesy.GeodeticCurve;
import org.gavaghan.geodesy.GlobalCoordinates;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/28 0:58
 * @file: Test
 * @description: 星期五
 */
public class DistanceUtil {

    /**
     *  point1: 坐标一，经度lng 纬度lat
     *  point2: 坐标二，经度lng 纬度lat
     * **/
    public static float getDistance(double[] point1,double[] point2){
        double X1 = point1[0];
        double X2 = point2[0];
        double Y1 = point1[1];
        double Y2 = point2[1];
        GlobalCoordinates source = new GlobalCoordinates(Y1,X1);
        GlobalCoordinates target = new GlobalCoordinates(Y2,X2);
        //以Sphere的方式计算
        Ellipsoid ellipsoid = Ellipsoid.Sphere;
        GeodeticCurve geodeticCurve = new GeodeticCalculator().calculateGeodeticCurve(ellipsoid,source,target);
        return (float)geodeticCurve.getEllipsoidalDistance()/1000;
    }
}
