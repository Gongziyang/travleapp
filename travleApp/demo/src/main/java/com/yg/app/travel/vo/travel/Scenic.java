package com.yg.app.travel.vo.travel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/25 21:24
 * @file: Senic
 * @description: 景点实体类
 */
@ApiModel(value = "景点实体")
public class Scenic {

    private int id;

    /**
     * 景点名称
     **/
    @ApiModelProperty(value = "景点名称")
    private String scenicName;

    /**
     * 景点状态
     **/
    @ApiModelProperty(value = "景点状态")
    private int state;

    /**
     * 推荐指数 1 2 3 4 5
     **/
    @ApiModelProperty(value = "推荐指数")
    private int recommendRate;
    /**
     * 景点门票
     **/
    @ApiModelProperty(value = "景点门票")
    private float ticket;
    /**
     * 景点经度
     **/
    @ApiModelProperty(value = "景点经度")
    private float lng;
    /**
     * 景点纬度
     **/
    @ApiModelProperty(value = "景点纬度")
    private float lat;
    /**
     * 景区图片，url
     **/
    @ApiModelProperty(value = "景区图片，url")
    private String picture;

    /**
     * 详细地址
     **/
    @ApiModelProperty(value = "详细地址")
    private String address;

    /**
     * 景区开放时间
     **/
    @ApiModelProperty(value = "开放时间")
    private String openTime;

    /**
     * 景区简介
     * **/
    @ApiModelProperty(value = "简介")
    private String introduce;

    /**
     * 景点特色
     * **/
    @ApiModelProperty(value = "景点特色")
    private String feature;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getScenicName() {
        return scenicName;
    }

    public void setScenicName(String scenicName) {
        this.scenicName = scenicName;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public float getTicket() {
        return ticket;
    }

    public void setTicket(float ticket) {
        this.ticket = ticket;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }


    public int getRecommendRate() {
        return recommendRate;
    }

    public void setRecommendRate(int recommendRate) {
        this.recommendRate = recommendRate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

}
