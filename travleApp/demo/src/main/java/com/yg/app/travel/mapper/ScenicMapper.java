package com.yg.app.travel.mapper;

import com.yg.app.travel.vo.req.ReqSenicInfo;
import com.yg.app.travel.vo.resp.RespSenicDetail;
import com.yg.app.travel.vo.travel.Comment;
import com.yg.app.travel.vo.travel.Scenic;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/26 16:08
 * @file: ScenicMapper
 * @description: 星期一
 */
@Mapper
public interface ScenicMapper {

    /**
     * 分页查询景点信息
     * **/
    List<Scenic> queryScnicPage(ReqSenicInfo request);

    /**
     * 查询景点详情，包括用户的评论信息
     * @param id 景点id
     * **/
    RespSenicDetail querySceicDetail(Integer id);

    int itemSize(ReqSenicInfo request);

    List<Scenic> queryTravelAround(Integer sorted);

    int comment(Comment comment);

}
