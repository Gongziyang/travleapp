package com.yg.app.travel.service.impl;

import com.yg.app.travel.mapper.ScenicMapper;
import com.yg.app.travel.service.ScenicService;
import com.yg.app.travel.vo.req.ReqSenicInfo;
import com.yg.app.travel.vo.resp.RespSenicDetail;
import com.yg.app.travel.vo.travel.Comment;
import com.yg.app.travel.vo.travel.Scenic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/26 16:14
 * @file: ScenicServiceImpl
 * @description: 星期一
 */
@Service
public class ScenicServiceImpl implements ScenicService {

    @Autowired
    ScenicMapper scenicMapper;

    @Override
    public List<Scenic> queryScnicPage(ReqSenicInfo request) {
        return scenicMapper.queryScnicPage(request);
    }

    @Override
    public RespSenicDetail querySceicDetail(int id) {
        return scenicMapper.querySceicDetail(id);
    }

    @Override
    public int itemSize(ReqSenicInfo request) {
        return scenicMapper.itemSize(request);
    }

    @Override
    public List<Scenic> queryTravelAround(Integer sorted) {
        return scenicMapper.queryTravelAround(sorted);
    }

    @Override
    public int comment(Comment comment) {
        return scenicMapper.comment(comment);
    }
}
