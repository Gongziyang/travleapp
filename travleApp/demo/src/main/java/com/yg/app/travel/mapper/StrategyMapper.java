package com.yg.app.travel.mapper;

import com.yg.app.travel.vo.travel.Strategy;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/1 19:16
 * @file: StrategyMapper
 * @description: 星期二
 */
@Mapper
public interface StrategyMapper {

    List<Strategy> queryStrategy();
}
