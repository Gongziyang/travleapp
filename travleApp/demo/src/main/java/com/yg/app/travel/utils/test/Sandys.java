package com.yg.app.travel.utils.test;

import java.io.PipedInputStream;
import java.util.ArrayList;

/**
 * @author: yyf
 * @create: 2021 06 2021/6/1 10:51
 * @file: Sandys
 * @description: 星期二
 */
public class Sandys {

    private int count;

    public static void main(String[] args) {
        Sandys sandys = new Sandys(99);
        System.out.println(sandys.count);

        sandys.test();
        ArrayList<String> list = new ArrayList<>();
        String s;
        try {
            s = "a";
            return;
        } catch (NullPointerException e) {
        } finally {
            System.out.println("finally");
        }
    }

    public Sandys(int c) {
        count = c;
    }

    public void test() {
        int i = 10;
        while (i > 0) {
            i = i + 1;
            if(i == 10){
                break;
            }
        }
    }
}
