package com.yg.app.travel.vo.travel;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.aspectj.weaver.ast.Or;

import javax.xml.crypto.Data;
import java.sql.Timestamp;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/25 21:25
 * @file: Order
 * @description: 订单信息实体
 */
@ApiModel(value = "订单信息")
public class Order {

    private int id;

    /**
     * 订单编号
     **/
    @ApiModelProperty(value = "订单编号")
    private String orderNumber;
    /**
     * 订单名称
     **/
    @ApiModelProperty(value = "订单名称")
    private String orderName;
    /**
     * 订单信息
     **/
    @ApiModelProperty(value = "订单信息")
    private String orderInfo;
    /**
     * 下单时间，用JsonFormat来转换为timestamp类型
     **/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    @ApiModelProperty(value = "下单时间")
    private String orderTime;
    /**
     * 应付金额
     **/
    @ApiModelProperty(value = "应付金额")
    private float accountPayable;

    /**
     * 订单状态
     **/
    @ApiModelProperty(value = "已支付0 待支付1")
    private int state;

    /**
     * 优惠卷使用
     **/
    @ApiModelProperty(value = "优惠卷使用")
    private float couponMoney;

    /**
     * 优惠卷ID
     * **/
    @ApiModelProperty(value = "优惠卷ID")
    private int couponId;
    /**
     * 关联用户
     **/
    @ApiModelProperty(value = "关联用户")
    private String userId;

    /**
     * 预计旅游时间
     **/
    @ApiModelProperty(value = "预计旅游时间")
    private String travelTime;


    /**
     * 是否使用优惠卷
     * **/
    @ApiModelProperty(value = "是否使用优惠卷")
    private int weatherUseCoupon;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getOrderInfo() {
        return orderInfo;
    }

    public void setOrderInfo(String orderInfo) {
        this.orderInfo = orderInfo;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public float getCouponMoney() {
        return couponMoney;
    }

    public void setCouponMoney(float couponMoney) {
        this.couponMoney = couponMoney;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTravelTime() {
        return travelTime;
    }

    public void setTravelTime(String travelTime) {
        this.travelTime = travelTime;
    }

    public float getAccountPayable() {
        return accountPayable;
    }

    public void setAccountPayable(float accountPayable) {
        this.accountPayable = accountPayable;
    }

    public int getWeatherUseCoupon() {
        return weatherUseCoupon;
    }

    public void setWeatherUseCoupon(int weatherUseCoupon) {
        this.weatherUseCoupon = weatherUseCoupon;
    }

    public int getCouponId() {
        return couponId;
    }

    public void setCouponId(int couponId) {
        this.couponId = couponId;
    }

    @Override
    public String toString() {
        return  "{"
                +"\""+"userId"+"\""+":"+"\""+this.userId+"\""+","
                +"\""+"orderName"+"\""+":"+"\""+this.orderName+"\""+","
                +"\""+"orderNumber"+"\""+":"+"\""+this.orderNumber+"\""+","
                +"\""+"accountPayable"+"\""+":"+this.accountPayable+","
                +"\""+"weatherUseCoupon"+"\""+":"+"\""+(this.couponMoney==0?"未使用":"已使用")+"\""+","
                +"\""+"couponMoney"+"\""+":"+this.couponMoney+","
                +"\""+"orderTime"+"\""+":"+"\""+this.orderTime+"\""+","
                +"\""+"travelTime"+"\""+":"+"\""+this.travelTime+"\""+","
                +"\""+"state"+"\""+":"+"\""+(this.state == 0?"未支付":"已支付")+"\""+","
                +"\""+"couponId"+"\""+":"+this.couponId+","
                +"\""+"orderId"+"\""+":"+this.id+
                "}";
    }
}
