package com.yg.app.travel.aop;
import com.google.gson.Gson;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/7 15:04
 * @file: LogAspect
 * @description: 星期三
 */
@Aspect
@Component
public class LogAspect {

    @Autowired
    Gson gson;

    static Logger logger = LoggerFactory.getLogger(LogAspect.class);

    @Pointcut("execution(public * com.yg.app.travel.controller.*.*(..))")
    public void myPoint() {
    }

    @Before("myPoint()")
    public void doBefore(JoinPoint joinPoint) {
        logger.info("【request】" + joinPoint.getSignature().getDeclaringTypeName() + "."
                + joinPoint.getSignature().getName());
        //当提交登出请求时暂时不获取请求数据，因为请求参数是reques,response
        if (!joinPoint.getSignature().getName().contains("logout")) {
            logger.info("【request-body】" + gson.toJson(joinPoint.getArgs()));
        }
    }

    @AfterReturning(value = "myPoint()()", returning = "result")
    public void doAfter(Object result) {
        logger.info("【response-body】" + gson.toJson(result));
    }

}
