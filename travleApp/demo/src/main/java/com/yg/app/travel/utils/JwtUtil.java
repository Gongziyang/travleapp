package com.yg.app.travel.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.yg.app.travel.common.CommonConstant;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

/**
 * @author: yyf
 * @create: 2021 03 2021/3/19 10:19
 * @file: JwtUtil
 * @description: 星期五
 */
public class JwtUtil {

    // 过期时间2个小时
    public static final long EXPIRE_TIME = 30 * 60 * 1000 * 4;

    /**
     * 校验token是否正确
     *
     * @param token  令牌
     * @param secret 运营商密钥
     * @return 是否正确
     */
    public static boolean verify(String token, String operationId, String secret) {
        try {
            // 根据密码生成JWT效验器
            Algorithm algorithm = Algorithm.HMAC256(secret);
            JWTVerifier verifier = JWT.require(algorithm).withClaim("operationId", operationId).build();
            // 效验TOKEN
            DecodedJWT jwt = verifier.verify(token);
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    /**
     * 获得token中的信息无需secret解密也能获得
     *
     * @return token中包含的用户名
     */
    public static String getOperatorId(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim("operationId").asString();
        } catch (JWTDecodeException e) {
            return null;
        }
    }

    /**
     * 生成签名,5min后过期
     *
     * @param operationId 运营商id
     * @param secret   运营商密钥
     * @return 加密的token
     */
    public static String sign(String operationId, String secret) throws UnsupportedEncodingException {
        Date date = new Date(System.currentTimeMillis() + EXPIRE_TIME);
        Algorithm algorithm = Algorithm.HMAC256(secret);
        // 附带operationId信息
        return JWT.create().withClaim("operationId", operationId).withExpiresAt(date).sign(algorithm);

    }

    /**
     * 根据request中的token获取运营商id
     * @param request
     * @return
     * @throws Exception
     */
    public static String getOperatorIdByToken(HttpServletRequest request) throws Exception {
        String accessToken = request.getHeader(CommonConstant.ACCESS_TOKEN);
        String operationId = getOperatorId(accessToken);
        BlankCheckUtil.isBlank(operationId,"未获取到运营商信息");
        return operationId;
    }

//    /**
//     * 从session中获取变量
//     *
//     * @param key
//     * @return
//     */
//    public static String getSessionData(String key) {
//        //${myVar}%
//        //得到${} 后面的值
//        String moshi = "";
//        if (key.indexOf("}") != -1) {
//            moshi = key.substring(key.indexOf("}") + 1);
//        }
//        String returnValue = null;
//        if (key.contains("#{")) {
//            key = key.substring(2, key.indexOf("}"));
//        }
//        BlankCheckUtil.isBlank(key,"key无效");
//            HttpSession session = BeanUtil.getHttpServletRequest().getSession();
//            returnValue = (String) session.getAttribute(key);
//        //结果加上${} 后面的值
//        if (returnValue != null) {
//            returnValue = returnValue + moshi;
//        }
//        return returnValue;
//    }

    public static void main(String[] args) throws NoSuchPaddingException, InvalidKeyException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException {
    }
}
