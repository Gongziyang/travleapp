package com.yg.app.travel.vo.travel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.sql.Timestamp;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/25 21:25
 * @file: Cupon
 * @description: 优惠卷信息实体
 */
@ApiModel(value = "优惠卷信息")
public class Coupon {

    private int id;

    /**
     * 优惠卷名字
     **/
    @ApiModelProperty(value = "优惠卷名字")
    private String couponName;

    /**
     * 优惠卷到期时间
     **/
    @ApiModelProperty(value = "优惠卷到期时间")
    private Timestamp expireTime;

    /**
     * 优惠金额
     * **/
    @ApiModelProperty(value = "优惠金额")
    private float couponMoney;

    /**
     * 用户ID
     * **/
    @ApiModelProperty(value = "用户ID")
    private String userId;

    /**
     * 优惠卷信息
     * **/
    @ApiModelProperty(value = "优惠卷信息")
    private int couponInfo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public Timestamp getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Timestamp expireTime) {
        this.expireTime = expireTime;
    }

    public int getCouponInfo() {
        return couponInfo;
    }

    public void setCouponInfo(int couponInfo) {
        this.couponInfo = couponInfo;
    }

    public float getCouponMoney() {
        return couponMoney;
    }

    public void setCouponMoney(float couponMoney) {
        this.couponMoney = couponMoney;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
