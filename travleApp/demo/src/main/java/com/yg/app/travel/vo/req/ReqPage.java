package com.yg.app.travel.vo.req;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/26 16:06
 * @file: ReqPage
 * @description: 星期一
 */
public class ReqPage {

    public int pageNo;

    public  int pageSize;

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
