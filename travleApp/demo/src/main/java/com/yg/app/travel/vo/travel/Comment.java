package com.yg.app.travel.vo.travel;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.factory.annotation.Value;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/25 21:25
 * @file: Comment
 * @description: 评论信息实体
 */
@ApiModel(value = "用户评论实体，关于景点/酒店的评论")
public class Comment {

    private int id;

    /**
     * 用户名，表示是谁评论的
     **/
    @ApiModelProperty(value = "用户名，表示是谁评论的")
    private String userName;
    /**
     * 评论时间，表示何时评论的
     **/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    @ApiModelProperty(value = "评论时间，表示何时评论的")
    private String commentTime;
    /**
     * 评论类型，表示评论的是酒店还是景区
     **/
    @ApiModelProperty(value = "评论类型，表示评论的是酒店还是景区")
    private int type;
    /**
     * 评论内容
     * **/
    @ApiModelProperty(value = "评论内容")
    private String content;

    /**
     * 好评等级
     * **/
    @ApiModelProperty(value = "好评等级")
    private int  satisfaction;

    /**
     * 关联景区ID
     * **/
    @ApiModelProperty(value = "关联景区ID")
    private int  refId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(String commentTime) {
        this.commentTime = commentTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getSatisfaction() {
        return satisfaction;
    }

    public void setSatisfaction(int satisfaction) {
        this.satisfaction = satisfaction;
    }

    public int getRefId() {
        return refId;
    }

    public void setRefId(int refId) {
        this.refId = refId;
    }
}