package com.yg.app.travel.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/20 9:57
 * @file: BeanUtil
 * @description: 星期二
 */
@Component
public class BeanUtil implements ApplicationContextAware {

    public  static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext apt) throws BeansException {
        applicationContext = apt;
    }

    public static Object getBean(String name){
        return applicationContext.getBean(name);
    }

    public static <T> T getBean(Class<T> z){
        return applicationContext.getBean(z);
    }

    public static <T> T getBean(Class<T> z,String name){
        return applicationContext.getBean(name,z);
    }
}
