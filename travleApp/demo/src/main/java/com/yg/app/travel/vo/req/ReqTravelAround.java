package com.yg.app.travel.vo.req;

/**
 * @author: yyf
 * @create: 2021 05 2021/5/28 23:49
 * @file: ReqTravelAround
 * @description: 星期五
 */
public class ReqTravelAround {

    private double lat;

    private  double lng;

    private int type;

    private int sorted;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getSorted() {
        return sorted;
    }

    public void setSorted(int sorted) {
        this.sorted = sorted;
    }
}
